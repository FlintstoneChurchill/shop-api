const request = require("supertest");
const mongoose = require("mongoose");
const config = require("../config");
const Product = require("../models/Product");
const { app, port } = require("../app");

describe("роут products", () => {
  const server = app.listen(port);

  beforeAll(async (done) => {
    await mongoose.connect("mongodb://localhost/" + config.db.name, { useNewUrlParser: true });
    done();
  });

  afterAll((done) => {
    mongoose.connection.close(() => done());
    server.close();
  });
  
  test("получение списка продуктов", async () => {
    const res = await request(app).get("/products");
    expect(res.statusCode).toBe(200);
    expect(res.body.length).toBe(2);
  });
  test("получение товара по id", async () => {
    const product = await Product.create({
      title: "test product",
      price: "200"
    });

    const res = await request(app).get("/products/" + product._id);
    expect(res.body.title).toBe("test product");
    await Product.findByIdAndRemove(product._id);
  });
  test("успешное создание нового товара", async () => {
    const userData = {
      username: "admin",
      password: "1@345qWert"
    };
    const user = await request(app).post("/users/sessions").send(userData);

    const product = {
      title: "test product",
      price: "200"
    };

    const res = await request(app).post("/products").send(product).set({ "Authorization": user.body.token });

    expect(res.statusCode).toBe(200);
    await Product.findByIdAndRemove(res.body._id);
  });
});