const path = require("path");

const rootPath = __dirname;

let dbName = "shop";

if (process.env.NODE_ENV === "test") {
  dbName = "shop_test";
}

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, "public/uploads"),
  db: {
    name: dbName,
    url: "mongodb://localhost"
  },
  fb: {
    appId: "670211216995860",
    appSecret: process.env.FB_SECRET
  }
};